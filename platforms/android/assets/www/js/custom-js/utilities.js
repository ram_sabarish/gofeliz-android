//var hostname = "http://gofelizraz9vuvrw9.devcloud.acquia-sites.com";

var hostname = "https://www.gofeliz.com";

var regixPhone = /^[0-9]{10}$/;

var regixEmail =/^[A-Za-z0-9._%+-]+[@][A-Za-z0-9.-]+[.][A-Za-z]{2,4}$/;

var category;

var categoryNID;

var subCategoryList;

var description;

var animate = true;

var islogin = false;

var orderNo = null;

var user_info = null;

var cityList = null;

var subCategoryNID;

var plumbingSubCategory = electricalsSubCategory = carpenterSubCategory = null;

var serviceRequestList = null;

var newRequestAdded = false;

var ticketDetails = null;

var offerImages = null;

var internetAvailable = true;

var isAppInitialize = false;

var upgradeNow = null;

var appVersionNumber = null;

//Base64 encoding service used by AuthenticationService
var Base64 = {
    
    keyStr: 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=',
    
    encode: function (input) {
        var output = "";
        var chr1, chr2, chr3 = "";
        var enc1, enc2, enc3, enc4 = "";
        var i = 0;
    
        do {
            chr1 = input.charCodeAt(i++);
            chr2 = input.charCodeAt(i++);
            chr3 = input.charCodeAt(i++);
        
            enc1 = chr1 >> 2;
            enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
            enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
            enc4 = chr3 & 63;
        
            if (isNaN(chr2)) {
                enc3 = enc4 = 64;
            } else if (isNaN(chr3)) {
                enc4 = 64;
            }
        
            output = output +
            this.keyStr.charAt(enc1) +
            this.keyStr.charAt(enc2) +
            this.keyStr.charAt(enc3) +
            this.keyStr.charAt(enc4);
            chr1 = chr2 = chr3 = "";
            enc1 = enc2 = enc3 = enc4 = "";
        } while (i < input.length);
    
        return output;
    },
    
    decode: function (input) {
        var output = "";
        var chr1, chr2, chr3 = "";
        var enc1, enc2, enc3, enc4 = "";
        var i = 0;
    
        // remove all characters that are not A-Z, a-z, 0-9, +, /, or =
        var base64test = /[^A-Za-z0-9\+\/\=]/g;
        if (base64test.exec(input)) {
            window.alert("There were invalid base64 characters in the input text.\n" +
                     "Valid base64 characters are A-Z, a-z, 0-9, '+', '/',and '='\n" +
                     "Expect errors in decoding.");
        }
        input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");
    
        do {
            enc1 = this.keyStr.indexOf(input.charAt(i++));
            enc2 = this.keyStr.indexOf(input.charAt(i++));
            enc3 = this.keyStr.indexOf(input.charAt(i++));
            enc4 = this.keyStr.indexOf(input.charAt(i++));
        
            chr1 = (enc1 << 2) | (enc2 >> 4);
            chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
            chr3 = ((enc3 & 3) << 6) | enc4;
        
            output = output + String.fromCharCode(chr1);
        
            if (enc3 != 64) {
                output = output + String.fromCharCode(chr2);
            }
            if (enc4 != 64) {
                output = output + String.fromCharCode(chr3);
            }
        
            chr1 = chr2 = chr3 = "";
            enc1 = enc2 = enc3 = enc4 = "";
        
        } while (i < input.length);
    
        return output;
    }
};


function getMonthNameUtilities(dateNow){
	
	switch( dateNow.getMonth() ){
    	case 0: return 'January';
    	case 1: return 'February';
    	case 2: return 'March';
    	case 3: return 'April';
    	case 4: return 'May';
    	case 5: return 'June';
    	case 6: return 'July';
    	case 7: return 'August';
    	case 8: return 'September';
    	case 9: return 'October';
    	case 10: return 'November';
    	case 11: return 'December';  
	}
	
	return "";
}

function getDayNameUtilites(dateNow){
	
	 switch(dateNow.getDay()){
	    case 0: return 'Sun';
	    case 1: return 'Mon';
	    case 2: return 'Tue';
	    case 3: return 'Wed';
	    case 4: return 'Thu';
	    case 5: return 'Fri';
	    case 6: return 'Sat'; 
	  }
	 
	  return '';
}

function getExpiresDate(date){

 var dateText = '';

 date = date.split('-');

 if(date.length != 3)
   return '-';

 var tempDate = new Date( date[2], parseInt(date[1])-1, date[0], 0, 0, 0, 0);

 dateText = tempDate.getDate() + ' ';

    switch( tempDate.getMonth() ){
     	case 0: dateText += 'Jan';
     	        break;
     	case 1: dateText += 'Feb';
     	        break;
     	case 2: dateText += 'Mar';
     	        break;
     	case 3: dateText += 'Apr';
     	        break;
     	case 4: dateText += 'May';
     	        break;
     	case 5: dateText += 'Jun';
     	        break;
     	case 6: dateText += 'Jul';
     	        break;
     	case 7: dateText += 'Aug';
     	        break;
     	case 8: dateText += 'Sep';
     	        break;
     	case 9: dateText += 'Oct';
     	        break;
     	case 10: dateText += 'Nov';
     	         break;
     	case 11: dateText += 'Dec';
 	}

 	dateText = dateText + ' ' + tempDate.getFullYear();

  return dateText;
}