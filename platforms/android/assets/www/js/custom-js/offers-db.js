var db;

function initDB(){

	db = window.openDatabase("GoFeliz", "1.0", "gofeliz", 200000);

	db.transaction(populateDB, errorCB, null);

}

function populateDB(tx) {

    tx.executeSql('CREATE TABLE IF NOT EXISTS Offers (PromoCode TEXT PRIMARY KEY, Title TEXT NOT NULL, Details TEXT NOT NULL , ExpiresIn TEXT)');

}


function errorCB(err) {

    //alert(JSON.stringify(err));
}


function insertOffers(data){

   db.transaction(function(tx){

   		 			tx.executeSql("INSERT INTO Offers ( PromoCode, Title, Details, ExpiresIn) VALUES (?,?,?,?)",[ data.code, data.title, data.details, data.expires ]);



   			},errorCB);
}


function getOffers(){

	db.transaction(function(tx){

					tx.executeSql('SELECT * FROM Offers', [], getOffersCallback, errorCB);

			},errorCB);

}


function getOffersCallback(tx, result){

   //alert(result.rows.length);
}