app.controller('bookingController',function($scope,$location,$http,$filter){
               
	animate = false;
	
	$scope.selectedDateText = 'Select Date';
	$scope.selectedTime = 'Select Time';
	$scope.selectedCityArea = {'title':'Select City Area'};
	
	var timeList = [ { 'id':'0', 'shift':'Morning','value':'9:00 AM - 11:00 AM'},
	                 { 'id':'1', 'shift':'Late Morning','value':'11:00 AM - 1:00 PM'},
	                 { 'id':'2', 'shift':'After Noon','value':'1:00 PM - 3:00 PM'},
	                 { 'id':'3', 'shift':'Late After Noon','value':'3:00 PM - 5:00 PM'},
	                 { 'id':'4', 'shift':'Evening','value':'5:00 PM - 7:00 PM'},
	                 { 'id':'5', 'shift':'Late Evening','value':'7:00 PM - 9:00 PM'}];
	
	$scope.timeSlots = timeList;
	
	$scope.cityAreas = cityList;
	
	/*Set Date*/ 
    var dateNow = new Date();   
    var datesList = new Array();
              
    for( i = 0 ; i < 8 ; i++){
   	 	     	     	     	 
    	datesList.push(new Date( dateNow.getTime() + i * 60 * 60 * 24 * 1000));
    }
    
    $scope.dates = datesList;
    /*End Set Date*/
	
	
    
    // Get Month Name 
	  $scope.getMonthName = function (dateNow){
		  
		  return getMonthNameUtilities(dateNow); //Method defined in Utilities.js	  
	 }
	// End Get Month Name
	  
	// Get Day Name  
	  $scope.getDayName = function(dateNow){
		  
		 return getDayNameUtilites(dateNow);  //Method defined in Utilities.js
	  }
	//End Get Day Name
	  
	  
	//Create Ticket
	$scope.createTicket = function(){
			
			var ticket = new Object();
			
			ticket.type = "service_ticket";
			
			if(islogin){
			
				ticket.uid = user_info.uid;
			}
			
			ticket.field_ticket_status = {und:[{value:"Open"}]};
			
			ticket.field_service_main_category = {und:[{target_id: categoryNID }]};
			
			ticket.field_service_description = {und:[{value: description }]};
			
			ticket.field_add_landmark = {und:[{value: $scope.landMark }]};
			
			ticket.field_building_flat_house_no_ = {und:[{value: $scope.building_Apartment_No }]};
			
			ticket.field_street = {und:[{value: $scope.street }]};
			
			ticket.field_city_area = {und:[{target_id: $scope.selectedCityArea.nid }]};
			
			ticket.field_scheduled_time = {und:[{value: $scope.selectedTimeID }]};
			
			ticket.field_scheduled_date = {und:[{value: $filter('date')($scope.selectedDate, "yyyy-MM-dd")}]};
			
			ticket.field_lead_source = {und:[{value: 'Mobile App Android' }]};
			
			if( $scope.couponCode != undefined &&  $scope.couponCode != "" ){

			     ticket.field_coupon = {und:[{value:$scope.couponCode}]};
			}

			
			if(subCategoryList.length > 0){
				   
				   var subCatTargetIds = [];
				   
				   for( i = 0; i < subCategoryList.length; i++){   
					   
						subCatTargetIds.push({"target_id":subCategoryList[i]});
				   }
				   
				   ticket.field_service_sub_category =  {und:subCatTargetIds};	   
			}

			ticketDetails = ticket;
			
			if(!islogin){
    			
    			$location.path('Login');
    			
    		}else{
    			
    			$('.loading-spinner').show();
    			
    			$http.post( [hostname, '/ws/entity_node'].join(""), ticket)
    			  .then(function(response) {
    			    					  			  
    				  if(response.statusText = 'OK'){
    				  
    				    if(response.data.field_ticket_status.und[0].value == 'Open'){
    				    	
    				      $('.loading-spinner').hide();
    						  
    				      orderNo = response.data.title;
    				      
    				      newRequestAdded = true;
    					 
    				      window.history.go(-2); 	
    				    }	 
    				  
    				  }
    				  
    			 	},function(error){
    			 	  
    			 	  $('.loading-spinner').hide();
    			 			
    			});
    			
    		}
            //End If Login
	}
	//End Create Ticket
	
	
	//Open Date Picker
	$scope.openDatePicker = function(){

	   $('#custom-time-picker').hide();

	   $('#custom-date-picker').show();
	 
	}
	//End Date Picker


	//Open Time Picker
	$scope.openTimePicker = function(){

		$('#custom-date-picker').hide();

		$('#custom-time-picker').show();

	}
	//End Time Picker

	
	 //On Select Time Slot
	 $scope.onSlectTimeSlot = function(optionTime){
	
		 $scope.selectedTime = optionTime.value != '-' ? optionTime.value : 'Select Time';
		 
		 $scope.selectedTimeID = optionTime.id;

		 $('#custom-time-picker').hide();
		 
	 }
	 //End On Select Time Slot


	//On Select Date List
	$scope.onSelectListdate = function(option){
		
		  $scope.selectedDate = option;
		  
		  $scope.selectedDateText = [option.getDate(), getMonthNameUtilities(option)].join(" ");

		  $('#custom-date-picker').hide();

		  $scope.refreshTimeSlots();
	}
	//End On Select Date List
	  
	  
	//Show Hide City Area List
	$scope.showHideCityAreaList = function(){
			
			if($scope.showCityAreasList)
				$scope.showCityAreasList = false;
			
			else
				$scope.showCityAreasList = true;
			
	}
	//End Show Hide City Area List


	//On Select City Area
	$scope.onSelectCityArea = function(option){

			$scope.showCityAreasList = false;

			$scope.selectedCityArea = option;
			
	}
    //End On Select City Area
		
	
	//Refresh Time Slots
	$scope.refreshTimeSlots = function(){
			
			if( dateNow.getTime() < $scope.selectedDate.getTime() ){
				$scope.timeSlots = timeList;
			    return;	
			}
									
			var haurs = parseInt( dateNow.getHours() );
									
			if( haurs <= 6 ){
				$scope.timeSlots = timeList;
				return;	
			}
			else if( haurs <= 8 ){
			     
			    $scope.timeSlots = [ { 'id':'1', 'shift':'Late Morning','value':'11:00 AM - 1:00 PM'},
			                         { 'id':'2', 'shift':'After Noon','value':'1:00 PM - 3:00 PM'},
			                         { 'id':'3', 'shift':'Late After Noon','value':'3:00 PM - 5:00 PM'},
			                         { 'id':'4', 'shift':'Evening','value':'5:00 PM - 7:00 PM'},
			                         { 'id':'5', 'shift':'Late Evening','value':'7:00 PM - 9:00 PM'}];	
			}
			else if( haurs <= 10 ){
			     
			    $scope.timeSlots = [ { 'id':'2', 'shift':'After Noon','value':'1:00 PM - 3:00 PM'},
			                         { 'id':'3', 'shift':'Late After Noon','value':'3:00 PM - 5:00 PM'},
			                         { 'id':'4', 'shift':'Evening','value':'5:00 PM - 7:00 PM'},
			                         { 'id':'5', 'shift':'Late Evening','value':'7:00 PM - 9:00 PM'}];	
			}
			else if( haurs <= 12 ){
			     
			    $scope.timeSlots = [ { 'id':'3', 'shift':'Late After Noon','value':'3:00 PM - 5:00 PM'},
			                         { 'id':'4', 'shift':'Evening','value':'5:00 PM - 7:00 PM'},
			                         { 'id':'5', 'shift':'Late Evening','value':'7:00 PM - 9:00 PM'}];	
			}
			else if( haurs <= 14 ){
			     
			    $scope.timeSlots = [ { 'id':'4', 'shift':'Evening','value':'5:00 PM - 7:00 PM'},
			                         { 'id':'5', 'shift':'Late Evening','value':'7:00 PM - 9:00 PM'}];	
			}
			else if( haurs <= 16 ){
			     
			    $scope.timeSlots = [ { 'id':'5', 'shift':'Late Evening','value':'7:00 PM - 9:00 PM'} ];	
			}
			else{
				
				$scope.timeSlots = [ { 'id':'6', 'shift':'No Slots Today','value':'-'} ];	
			}
			
			$scope.selectedTime = 'Select Time';
	}
    //End Refresh Time Slots


	//Validation
	$scope.validateDetails = function(){
			
			var validation = true;

			$scope.showCityAreasList = false;

			if( $scope.selectedDateText == 'Select Date' && $scope.selectedTime == 'Select Time' ){
				
				$scope.dateTimeError = 'Please select date & time.';
				
				validation = false;
				
				return;
			}
			else if( $scope.selectedDateText == 'Select Date' ){
				
                $scope.dateTimeError = 'Please select date.';
				
				validation = false;
				
				return;
			}
			else if( $scope.selectedTime == 'Select Time' ){
				
				$scope.dateTimeError = 'Please select time.';
					
				validation = false;
				
				return;
			}
			else{
				
				$scope.dateTimeError = false;
			}
			
			
			
			if($scope.building_Apartment_No == undefined){
				
				$scope.building_Apartment_No_Error = 'Please specify Building/Apartment no.';
				
				validation = false;
				
				return;
			}
			else{
				
				$scope.building_Apartment_No_Error = false;
			}
			
			
			if($scope.street == undefined){
				
				$scope.streetError = 'Please specify street.';
				
				validation = false;
				
				return;
			}
			else{
				
				$scope.streetError = false;
			}
			
			
            if($scope.landMark == undefined){
				
				$scope.landMarkError = 'Please specify landMark.';
				
				validation = false;
				
				return;
			}
			else{
				
				$scope.landMarkError = false;
			}
            
            
            if($scope.selectedCityArea.title == 'Select City Area'){
				
				$scope.selectedCityAreaError = 'Please specify city area.';
				
				validation = false;
				
				return;
			}
			else{
				
				$scope.selectedCityAreaError = false;
			}
                   				
    		$scope.createTicket();
    			
	}
	//End Validation
               
});
//End Controller