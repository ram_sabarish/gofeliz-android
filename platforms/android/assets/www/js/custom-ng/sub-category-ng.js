app.controller('subCategoryController',function($scope,$http,$location){
	
	//Get Sub Categories
	$scope.getSubCategories = function(subCategoryArray){
						
		var url = [ hostname, '/ws/entity_node?parameters[field_main_category]=',  $scope.nid, '&parameters[type]=service_sub_category&fields=nid,title,field_main_category'].join("");
	
		 $http.get(url,{}).
         then(function(response){
        	       //alert(JSON.stringify(response));
        	       if(response.statusText = 'OK'){
        	    	   var subCategory;
        	    	   var subCategories = new Array();
        	    	   
        	    	   for( i = 0 ; i < response.data.length ; i++){
        	    		   subCategory = new Object();
        	    		   subCategory.title = response.data[i].title;
        	    		   subCategory.nid =response.data[i].nid;
        	    		   subCategories.push(subCategory);
        	    		   subCategoryArray.push(subCategory);
        	    	   }
        	    	   
        	    	   $('.loading-spinner').hide();
        	    	        	    	          	    	   
        	    	   $scope.subCategories = subCategories; 
        	       }
               },
               
               function(error){
            	   $('.loading-spinner').hide();
            	   //alert(JSON.stringify(error)); 
         });
		 
		 setTimeout(function(){
			 
			 if($scope.subCategories == null){
				 $('.loading-spinner').show();
			 } 
			 
		 },1000);
		 
		    
		 
	}
	//End Get Sub Categories
	
	
	 //Initialize page categories
	 $scope.initSubCategory = function(){
		 
		 if(animate){
			$scope.pageClass = 'slideLeft';
		 }
		 else{
			animate = true;
		 }
		 
		 $scope.subCategories = null;
		 
		 switch(category){
		 
		  case 1: $scope.categoryName = 'Plumbing';
		          $scope.nid = categoryNID = '40';
		          
		          if( plumbingSubCategory == null ){
		        	  
		        	  plumbingSubCategory = new Array();
		        	  $scope.getSubCategories(plumbingSubCategory);
		          }
		          else{
		        	  
		        	  $scope.subCategories = plumbingSubCategory;
		          }
		          
		          break;
		 
		  case 2: $scope.categoryName = 'Electricals';
		          $scope.nid = categoryNID ='190';
		          
		          if( electricalsSubCategory == null){
		        	  
		        	  electricalsSubCategory = new Array();
		        	  $scope.getSubCategories( electricalsSubCategory );
		          }
		          else{
		        	  
		        	  $scope.subCategories = electricalsSubCategory;
		          }
		          
		          break;
		 
		  case 3: $scope.categoryName = 'Carpentry';
		          $scope.nid = categoryNID ='256';
		          
		          if( carpenterSubCategory == null){
		        	  
		        	  carpenterSubCategory = new Array();
		        	  $scope.getSubCategories( carpenterSubCategory );
		          }
		          else{
		        	  
		        	  $scope.subCategories = carpenterSubCategory;
		          }
		          
		          break;
		 
		 }

		
	 }
	//End Initialize page categories
	 
	 $scope.validateDetails = function(){
		 
		 if($scope.description != undefined && $scope.description != ''){
			 $scope.valid = true;
		 }
		 
		 else if( subCategoryList.length != 0){
			 $scope.valid = true;
		 }
		 else{
			 $scope.valid = false;
		 }
		 
	 }
   
	 
	 //Select SubCategory
	 $scope.onSelectSubCategory = function( event, nid ){
		 
		 if($(event.target).hasClass('item-active')){
			 
			 $(event.target).removeClass('item-active');
			 
			 var index = subCategoryList.indexOf(nid);
			 
			 if(index > -1){
				
				 subCategoryList.splice(index, 1);
			 }
		 }
		 else{
			 
			 $(event.target).addClass('item-active'); 
			 
			 subCategoryList.push(nid);
		 }
		 
		 $scope.validateDetails();
	 }
	 //End Select SubCategory
	 
	
	 
	 
	 $scope.proceedBooking = function(){
		 
		 description = $scope.description != undefined ? $scope.description: '';
		 
		 $location.path('Booking'); 
	 }
	 
	 $scope.initSubCategory();
	 
	 subCategoryList = new Array(); 
	 
	 description = '';
});