app.controller('ordersController',function($scope,$http,$location){
	
	$scope.pageClass = 'slideUp';
	$scope.userLoggedIn = islogin;
	$scope.orderList = null;
	animate = false;
	
	//http://gofelizraz9vuvrw9.devcloud.acquia-sites.com/ws/entity_node?parameters[type]=service_ticket&parameters[uid]=416&fields=title,nid,field_ticket_status,field_service_description,created,field_add_landmark,field_building_flat_house_no_,field_street,field_city_area,field_scheduled_time,field_scheduled_date,field_service_main_category,field_service_sub_category
	
	if(islogin){
		
	   /*	
  	   var url = [ hostname, '/ws/entity_node?parameters[type]=service_ticket&parameters[uid]=',
  	               user_info.uid, '&fields=title,nid,field_ticket_status,field_service_description,created'].join("");
  	   */
  	   var flag = true;

		if(serviceRequestList == null || newRequestAdded){
			
			newRequestAdded = false;
			
			var url = [ hostname, '/ws/entity_node?parameters[type]=service_ticket&parameters[uid]=',
			            user_info.uid,'&fields=title,nid,field_ticket_status,field_scheduled_date'].join("");
			
	  	   $http.get(url, {}).then(function(response) {
	             
	  		   $('.loading-spinner').hide();
	  		   
	  		   serviceRequestList = new Array();
	  		   var order;
	  		   
	  		   for( i = 0; i < response.data.length; i++){
	  			   
	  			    order = new Object();
	  			    order.srn = response.data[i].title;
	  			    order.nid = response.data[i].nid;
	  			    order.scheduled_date = response.data[i].field_scheduled_date.und[0].value;
	  			    order.status = response.data[i].field_ticket_status.und[0].value;
	  			    
	  			  serviceRequestList.push(order); 
	  		   }
	  		   
	  		   $scope.orderList = serviceRequestList;
	         
	          },
	          function(error){
	         
	             $('.loading-spinner').hide();

	             flag = false;

	          	 $scope.noRecords = true;
	             //alert(JSON.stringify(error));
	             
	        }); 
	  	   
	  	    setTimeout(function(){
			 
			 if( $scope.orderList == null && flag){
				 $('.loading-spinner').show();
			 } 
			 
		   },1000);
	  	   
		}
		else{
			
			$scope.orderList = serviceRequestList;
			
		}
  	   //End If serviceRequestList
  	    
  	 
		
	}
	
	//Get Formatted Date
    $scope.getFormattedDate = function(dateString){
 	
 	   //var createdDate = new Date(parseInt(dateString));
    	
       var createdDate = new Date(dateString);
 	   
 	   return [ getDayNameUtilites(createdDate), createdDate.getDate(), getMonthNameUtilities(createdDate), [createdDate.getYear(),""].join("").substring(1,3) ].join(" ") ;
 	   
    }
   //End Get Formatted Date
    
    
    //On Select Order
    $scope.onSelectOrder = function(nid){
    	
    	subCategoryNID = nid; 
    	$location.path('OrderDetail');
    } 
    //End On Select Order
	
});