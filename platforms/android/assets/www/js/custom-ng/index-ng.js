var app = angular.module('gofeliz',['ngRoute','ngAnimate','angular-carousel']);

//Routing Configuration
app.config(function($routeProvider){
    
    $routeProvider.when('/',{
                        
                        templateUrl: 'html/home-new.html',
                        controller: 'homeController'   
                        	
                 }).when('/Account',{
                        
                        templateUrl: 'html/account-new.html',
                        controller: 'accountController'
                        
                }).when('/SubCategory',{
                    
                    templateUrl: 'html/sub-categories.html',
                    controller: 'subCategoryController'
                            
               }).when('/Settings',{
                    
                    templateUrl: 'html/settings-new.html',
                    controller: 'settingsController'
                            
               }).when('/Login',{
                   
                   templateUrl: 'html/login-new.html',
                   controller: 'loginController'
                           
              }).when('/Booking',{
                  
                  templateUrl: 'html/book-service.html',
                  controller: 'bookingController'
                          
             }).when('/OrderDetail',{
                 
                 templateUrl: 'html/order-detail.html',
                 controller: 'orderDetalsController'
                 
             }).when('/Offers',{

                                templateUrl: 'html/offer-new.html',
                                controller: 'offersController'

             }).when('/Order',{
                    
                    templateUrl: 'html/order-new.html',
                    controller: 'ordersController'
                    
            });

});
//End Routing Configuration 



app.controller('mainController',function($scope,$http){
	
  //  var authdata = Base64.encode('internal:gofeliz2015');
	
  //  $http.defaults.headers.common['Authorization'] = ['Basic', authdata].join(" ");
	
	$http.post(hostname + "/services/session/token",{}).
	then( function(response){
			
		     $http.defaults.headers.common['X-CSRF-Token']= response.data;
		     		     		
		     $('.loading-spinner').hide();
		     
		     return $http.get(hostname +'/ws/entity_node?parameters[field_city_city]=38&parameters[type]=city_areas&fields=nid,title', {});
	}).
	then( function(response){
		
		if(response.statusText = 'OK')
			cityList = response.data;
	}).
	catch(function(error){
		
		$('.loading-spinner').hide();
		
		//alert(JSON.stringify(error));
	});
	
	
	/*Check User Logged in status*/
	if(window.localStorage.uid == undefined){
	    isUserLoggedIn = false;
	}
	else{
		restoreUserCredentials();
	}
    /*End Check User Logged in status*/


    $scope.onBackPress = function(){

      alert('Back');
    }
	
});