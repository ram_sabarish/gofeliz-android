function goBack(){
    window.history.back();
}



function sidePanelToggle(){
	  
	if($("#home-container").hasClass("push-container")){
		
		$("#home-container").addClass("pull-container");
		$('#home-container').removeClass('push-container');
		
		$("#home-side-panel").addClass("side-panel-inactive");
		$('#home-side-panel').removeClass('side-panel-active');
		
		$('#homeside-panel-overlay').removeClass('side-panel-overlay-active');
		$("#homeside-panel-overlay").addClass("side-panel-overlay-inactive");
		
	}else{
		
		$("#home-container").removeClass("pull-container");
		$('#home-container').addClass('push-container');
		
		$("#home-side-panel").addClass("side-panel-active");
		$('#home-side-panel').removeClass('side-panel-inactive');
		
		$('#homeside-panel-overlay').removeClass('side-panel-overlay-inactive');
		$("#homeside-panel-overlay").addClass("side-panel-overlay-active");
		
		
	}
	
	
	
  }

function removeSidePanel(){
	
	$("#home-container").addClass("pull-container");
	$('#home-container').removeClass('push-container');
	
	$("#home-side-panel").addClass("side-panel-inactive");
	$('#home-side-panel').removeClass('side-panel-active');
	
	$('#homeside-panel-overlay').removeClass('side-panel-overlay-active');
	$("#homeside-panel-overlay").addClass("side-panel-overlay-inactive");
	
}

function storeUserCredentials(){
	window.localStorage.uid = user_info.uid;
	window.localStorage.firstName = user_info.firstName;
	window.localStorage.lastName = user_info.lastName ;
	window.localStorage.email = user_info.email;
	window.localStorage.phoneNumber = user_info.phoneNumber;
	window.localStorage.password = user_info.password;	
}

function removeUserCredentials(){
	user_info = null;
	islogin = false;
	delete window.localStorage["uid"];
	delete window.localStorage["firstName"];
	delete window.localStorage["lastName"];
	delete window.localStorage["email"];
	delete window.localStorage["phoneNumber"];
	delete window.localStorage["password"];
}

function restoreUserCredentials(){
	islogin = true;
	user_info = new Object();
	user_info.uid = window.localStorage.uid;
	user_info.firstName = window.localStorage.firstName;
	user_info.lastName = window.localStorage.lastName ;
	user_info.email = window.localStorage.email;
	user_info.phoneNumber = window.localStorage.phoneNumber;
	user_info.password = window.localStorage.password;
}


function checkConnection() {
    var networkState = navigator.connection.type;

    var states = {};
    states[Connection.UNKNOWN]  = 'Unknown connection';
    states[Connection.ETHERNET] = 'Ethernet connection';
    states[Connection.WIFI]     = 'WiFi connection';
    states[Connection.CELL_2G]  = 'Cell 2G connection';
    states[Connection.CELL_3G]  = 'Cell 3G connection';
    states[Connection.CELL_4G]  = 'Cell 4G connection';
    states[Connection.CELL]     = 'Cell generic connection';
    states[Connection.NONE]     = 'No network connection';

    if(states[networkState] == 'No network connection'){
    	
    	$('#home-page').hide();
    	$('#no-internet').show();
    	internetAvailable = false;
    	
    }else{
    	
    	$('#no-internet').hide();
    	$('#home-page').show();
    	internetAvailable = true;

    	InitNotifications();
    }
  
}

function initializeApp(){

	checkConnection();
	initDB();

}


function InitNotifications(){

 if(isAppInitialize){
     return;
 }

 isAppInitialize = true;

 var push = PushNotification.init({
     android: {
         senderID: "1064535524309",
          topics: ["gofeliz_promo_app"],
          vibrate: true
     },
     ios: {},
     windows: {}
 });

 push.on('registration', function(data) {

     registrationToken = data.registrationId;

 });

 push.on('notification', function(data) {
     // data.message,
     // data.title,
     // data.count,
     // data.sound,
     // data.image,
     // data.additionalData

     $(".app-promo").html(['<a href="#Offers" style="color:#ffffff;">',data.message,'</a>'].join(''));

     $(".app-promo").removeClass('app-promo-inactive');
     $(".app-promo").addClass('app-promo-active');


     $(".header").removeClass('header-active');
     $(".header").addClass('header-inactive');


     setTimeout(function(){

          $(".app-promo").removeClass('app-promo-active');
          $(".app-promo").addClass('app-promo-inactive');

          $(".header").removeClass('header-inactive');
          $(".header").addClass('header-active');

     },6000);


     if( data.additionalData.code != undefined && data.title != undefined && data.additionalData.details != undefined && data.additionalData.expires != undefined){

       var message_data = { "code": data.additionalData.code,
                            "title":data.title,
                            "details":data.additionalData.details,
                            "expires":data.additionalData.expires };

       insertOffers(message_data);

     }

 });

 push.on('error', function(e) {
     // e.message

 });

}


document.addEventListener("deviceready", initializeApp, false);



document.addEventListener("online", checkConnection, false);
document.addEventListener("offline", checkConnection, false);

/*
function removeSidePanelsZIndex(){
	
	 $(".side-panel").css({"z-index": "-1"});

	 $(".side-panel-overlay").css({"z-index": "-1"});	
}


function addSidePanelsZIndex(){
	
	   $(".side-panel").css({"z-index": "1"});
	   
	   $(".side-panel-overlay").css({"z-index": "1"});
}

function sidePanelToggle(){
	   
    if($(".side-panel").hasClass("side-panel-active")){
    	
       $(".side-panel").addClass("side-panel-inactive");
       $(".side-panel").removeClass("side-panel-active");
       
       $(".side-panel-overlay").addClass("side-panel-overlay-inactive");
       $(".side-panel-overlay").removeClass("side-panel-overlay-active");
       
     }else{
         $(".side-panel").removeClass("side-panel-inactive");
         $(".side-panel").addClass("side-panel-active");
         
         $(".side-panel-overlay").removeClass("side-panel-overlay-inactive");
         $(".side-panel-overlay").addClass("side-panel-overlay-active");
     }
    
  }

function removeOverlayAndSidePanel(){
 	  
	$(".side-panel").addClass("side-panel-inactive");
    $(".side-panel").removeClass("side-panel-active");
 
    $(".side-panel-overlay").addClass("side-panel-overlay-inactive");
    $(".side-panel-overlay").removeClass("side-panel-overlay-active");
	  
} 


function animateHideAllOptions(){
    $(".home-options").removeClass("home-options-active");
    $(".home-options").addClass("home-options-in-active");
}

function animateHomeOption(option){
	
	removeOverlayAndSidePanel();
	
	setTimeout(function(){
	
		if(!$(option).hasClass("home-options-active")){
		
		    animateHideAllOptions();
		
		    $(option).removeClass("home-options-in-active");
        
		    $(option).addClass("home-options-in-active-bottom"); 
        
            setTimeout(function(){
        	   $(option).addClass("home-options-active");
        	   $(option).removeClass("home-options-in-active-bottom");
        	
            },100);  
	    }
	},500);		
	
}




function initializePage(){
	var images = ['background-work-bench.jpg','background-city.jpg','background-home.jpg','background-night.jpg','background-pc.jpg','background-table.jpg','background.jpg'];
	var url;
	var i = 1;
		
	setInterval(function(){
			  
		   url = [ 'url(img/', images[i++], ')' ].join('');
		
		   $('#home').css({'background': url});
		
		   $('#home').css({'background-size': '100% 100%'});
		
		   if(i == images.length)
			  i = 0;
		
	},30000);
}
*/

//document.addEventListener("deviceready", onDeviceReady, false);
//
//function onDeviceReady() {
//	
//	navigator.notification.alert(
//		    'You are the winner!',  // message
//		    null,         // callback
//		    'Game Over',            // title
//		    'Done'                  // buttonName
//		);
//}