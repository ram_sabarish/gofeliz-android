app.controller('ordersController',function($scope,$http){
               
               $scope.pageClass = 'Orders';
               
               $scope.userLoggedIn = isUserLoggedIn;
               
               if(isUserLoggedIn){
            	   
            	   $('.loading-spinner').show();
            	   
            	   var url = [ hostname, '/ws/entity_node?parameters[type]=service_ticket&parameters[uid]=',
            	               user_info.uid, '&fields=title,nid,field_ticket_status,field_service_description,created'].join("");
            	
            	   $http.get(url, {}).then(function(response) {
                       
            		   $('.loading-spinner').hide();
            		   
            		   var orderList = new Array();
            		   var order;
            		   
            		   for( i = 0; i < response.data.length; i++){
            			   
            			    order = new Object();
            			    order.srn = response.data[i].title;
            			    order.nid = response.data[i].nid;
            			    order.created = response.data[i].created;
            			    order.status = response.data[i].field_ticket_status.und[0].value;
            			    
            			    orderList.push(order); 
            		   }
            		   
            		   $scope.orderList = orderList;
                   
                    },
                    function(error){
                   
                       $('.loading-spinner').hide();
                    	
                       alert(JSON.stringify(error));
                       
                  });   
            	   
               }
              //End of isUserloggedIn
               
              
              //Get Formatted Date
               $scope.getFormattedDate = function(dateString){
            	
            	   
            	   var createdDate = new Date(parseInt(dateString));
            	   
            	   return [ getDayNameUtilites(createdDate), createdDate.getDate(), getMonthNameUtilities(createdDate), createdDate.getYear() ].join(" ") ;
            	   
               }
              //End Get Formatted Date
               
               
              
                             
});
//End of ordersController