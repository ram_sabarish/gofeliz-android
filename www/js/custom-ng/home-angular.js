app.controller('homeController',function($scope,$location,$http,$filter){
              
			   $scope.pageClass = 'Home';
			   $scope.step = 'category';
			   $scope.dates = dates;
			   $scope.selectedDate = dates[0];
			   $scope.showConfirm = true;
			   $scope.showSelectedTime = true;
			   $scope.showSelectedDate = true;
			   $scope.listTimes = false;
			   $scope.showSelectCityAreas = false;
			   $scope.selectedCityArea = {'title':'Select City Area','nid':'50'};
			   $scope.ticketDescription = '';
			   $scope.ticketSRN = '';
			   
			   addSidePanelsZIndex();
			   			   
			   $scope.username = user_info == null ? 'Account' : user_info.firstName;
			   
			 
			   /*Function to check login status*/
			   $scope.login = function(){
				   
				   if(isUserLoggedIn){
					   
					  $('#logout-modal').modal('show');
				   }
				   else{
					   
					  removeSidePanelsZIndex();
					  
					  $location.path('login'); 
				   }
				   
			   }
			   //End function check login status
			   
			   
			   /*Function to logout*/
				 $scope.logout = function(){
					 
					 $('.loading-spinner').show();
					 
					 var user_data =  {			
								username: user_info.phoneNumber,
								password: user_info.password
								};
					 
					 $http.post( hostname + '/ws/user/logout', user_data).
			         then(function(response){ 
			          
			           $('.loading-spinner').hide();
			           
			           $scope.username = 'Account';
			           
			           removeUserCredentials();
			        	 
			         },function(error){
			        	 
			        	 $('.loading-spinner').hide();
			        	 
			        	 $scope.username = 'Account';
				           
				         removeUserCredentials();
			        	 			        	 
			         });
					 
					 
				 }
			  /*End Function logout*/
				 
			  /*Function Get Sub Categories*/
			  $scope.showSubCategory = function(category){
				 var url;
				 				 
				 $scope.timeSlots = timeSlots;
				 $scope.selectedTime = timeSlots[0];
				 $scope.cityAreas = cityAreas;
				  
	
				 $('.loading-spinner').show();			 
				 for( i = 0 ; i < categories.length ; i++){
					 if(categories[i].title == category){
						 $scope.selectedCategory = categories[i];
						 break;
					 }
				 }  
				 
				 url = [ hostname, '/ws/entity_node?parameters[field_main_category]=', $scope.selectedCategory.nid, '&parameters[type]=service_sub_category&fields=nid,title,field_main_category'].join("");
				  
                 $http.get(url,{}).
                 then(function(response){
                	       //alert(JSON.stringify(response));
                	       if(response.statusText = 'OK'){
                	    	   var subCategory;
                	    	   var subCategories = new Array();
                	    	   
                	    	   for( i = 0 ; i < response.data.length ; i++){
                	    		   subCategory = new Object();
                	    		   subCategory.title = response.data[i].title;
                	    		   subCategory.nid =response.data[i].nid;
                	    		   subCategories.push(subCategory);
                	    	   }
                	    	   
                	    	   $('.loading-spinner').hide();
                	    	   $scope.step = 'sub-category';
                	    	   $scope.showBack = true;
                	    	   //alert(JSON.stringify(subCategories));
                	    	   
                	    	   $scope.subCategories = subCategories; 
                	       }
                       },
                       function(error){
                    	   $('.loading-spinner').hide();
                    	   alert(JSON.stringify(error)); 
                 });				  
			  }	 
			  /*End Get Sub Categories*/
			  
			  $scope.subCategoryListSelect = function(option){
				  
				  $scope.selectedSubCategory = option;
				  $scope.step = 'date-time';
			  }
			  
			  
			  $scope.subCategoryNext = function(){
				  $scope.step = 'date-time';
			  }
			  
			  // Show Hide Date List
			  $scope.showListDates = function(){
				 			 
				 if($scope.listDates){
					 
					 $scope.listDates = false;
					 $scope.showConfirm = true;
					 $scope.showSelectedTime = true;
					 
				 } 
				 else{
					 $scope.listDates = true;
					 $scope.showConfirm = false;
					 $scope.showSelectedTime = false;
				 }
				 $scope.listTimes = false;
			  }
			 //End Show Hide Date List     
			  
			  
			  // On Select Date List
			  $scope.onSelectListdate = function(option){
				  $scope.listDates = false;
				  $scope.selectedDate = option;
				  $scope.showConfirm = true;
				  $scope.showSelectedTime = true;
			  }
			 //End On Select Date List			 
			  

			 //Show Hide Times
			 $scope.showListTimes = function(){
				 if($scope.listTimes){
					 $scope.listTimes = false;
					 $scope.showConfirm = true;
					 $scope.showSelectedDate = true;
				 } 
				 else{
					 $scope.listTimes = true;
					 $scope.showConfirm = false;
					 $scope.showSelectedDate = false;
				 }	  
			 }
			 //End Show Hide Times 
			 
			 //On Select Time Slot
			 $scope.onSlectTimeSlot = function(optionTime){
				 $scope.listTimes = false;
				 $scope.selectedTime = optionTime;
				 $scope.showConfirm = true;
				 $scope.showSelectedDate = true;
			 }
			 //End On Select Time Slot
			 
			
			 
			 // Get Month Name 
			  $scope.getMonthName = function (dateNow){
				  
				  return getMonthNameUtilities(dateNow); //Method defined in Utilities.js
				  
			 }
			// End Get Month Name
			  
			// Get Day Name  
			  $scope.getDayName = function(dateNow){
				  
				 return getDayNameUtilites(dateNow);  //Method defined in Utilities.js 
				  
			  }
			//End Get Day Name
			
			  
			//Get Time Slot Name  
			$scope.getTimeSlotName = function(time){
			
				var timeSlot =  (time + '').split('(');
				//forcibly converting time to string and splitting it
				
				return timeSlot[0];
			} 
			//End Get Time Slot Name
			
			//Get Time Slot Name  
			$scope.getTimeSlotvalue = function(time){
			
				var timeSlot =  (time + '').split( '(' );
				//forcibly converting time to string and splitting it
				
				return (timeSlot[1] + '').replace( ')' , '' );
				//forcibly converting timeSlot to string and replacing it
			} 
			//End Get Time Slot Name 
			
			
			//Confirm Date & Time
			$scope.dateTimeConfirm = function(){
				
				$scope.step = 'address';
			}
			//End Confirm Date & time
			
			// Show Hide City Area List
			$scope.showHideCityAreaList = function(){
				
				if($scope.showCityAreasList)
					$scope.showCityAreasList = false;
				
				else
					$scope.showCityAreasList = true;
				
			}
			// End Show Hide City Area List
			
			// On Select City Area
			$scope.onSelectCityArea = function(option){
				$scope.showCityAreasList = false;
				$scope.selectedCityArea = option;
			}
			//End On Select City Area
			
			// Address Form Next
			$scope.addressNext = function(){
				
				if(isUserLoggedIn)
					$scope.createTicket();
				else	
				  $scope.step = 'user-details';
			}
			//End Address Form Next
			
			
			// Step Back
			$scope.onStepBack = function(){
				switch($scope.step){
				 
				  case 'sub-category'   : $scope.step = 'category';
				                          $scope.showBack = false;
				                          break;
				                        
				  case 'date-time'      : $scope.step = 'sub-category';
				                          break; 
				                     
				  case 'address'        : $scope.step = 'date-time';
				                          break; 
				                   
				  case 'user-details'   : $scope.step = 'address';
                                          break; 
                                        
				  case 'ticket-reciept' : $scope.step = 'category';
				                          $scope.showBack = false;
                  						  break;                       
				
				}
			}
			//End Step Back
			
			
			//Create Ticket
			$scope.createTicket = function(){
				
				
				var ticket = new Object();
				
				ticket.type = "service_ticket";
				
				ticket.uid = user_info.uid;
				
				ticket.field_ticket_status = {und:[{value:"Open"}]};
				
				ticket.field_service_main_category = {und:[{target_id: $scope.selectedCategory.nid }]};
				
				ticket.field_service_sub_category = {und:[{target_id: $scope.selectedSubCategory.nid }]};
				
				ticket.field_service_description = {und:[{value: $scope.ticketDescription }]};
				
				ticket.field_add_landmark = {und:[{value: $scope.landMark }]};
				
				ticket.field_building_flat_house_no_ = {und:[{value: $scope.building_Apartment_No }]};
				
				ticket.field_street = {und:[{value: $scope.street }]};
				
				ticket.field_city_area = {und:[{target_id: $scope.selectedCityArea.nid }]};
				
				ticket.field_scheduled_time = {und:[{value: $scope.selectedTime.id }]};
				
				ticket.field_scheduled_date = {und:[{value: $filter('date')($scope.selectedDate, "yyyy-MM-dd")}]};
				
				/*
				alert(JSON.stringify(ticket));
				
				var ticket_details = {
						 type:"service_ticket", 
						 uid: $scope.data.userLog.user.uid,
						 field_ticket_status:{und:[{value:"Open"}]},
						 field_service_main_category:{und:[{target_id:$scope.data.selectedCategoryId}]},
						 field_service_sub_category:{und:[{target_id:$scope.data.selectedSubCategory.nid}]},
						 field_service_description:{und:[{value:$scope.data.ticketDescription}]},
						 //field_service_address:[],
						 field_add_landmark:{und:[{value:$scope.data.address.landMark}]},
						 field_city_area:{und:[{target_id:$scope.data.selecteCityArea.nid}]},
						// "field_city":{"und":[{"target_id":"38"}]},
						// "field_postalcode":[],
						 field_building_flat_house_no_:{und:[{value:$scope.data.address.flatBuilding}]},
						 field_street:{und:[{value:$scope.data.address.street}]},
						 field_scheduled_date:{und:[{value: $filter('date')($scope.data.scheduledDate, "yyyy-MM-dd")}]},
						 field_scheduled_time:{und:[{value:$scope.data.selectedTimeSlot.id}]} 
						 };

				*/
				
				$('.loading-spinner').show();
				
				$http.post(hostname + '/ws/entity_node', ticket)
				  .then(function(response) {
				    	
					  //alert(JSON.stringify(response));
					  
					  if(response.statusText = 'OK'){
					  
					    if(response.data.field_ticket_status.und[0].value == 'Open'){
					    	
					      $('.loading-spinner').hide();
							  
						  $scope.step = 'ticket-reciept';
						  
						  $scope.ticketSRN = response.data.title;
					    	
					    }	 
					  
					  }
					  
				 	},function(error){
				 		
				 	  alert(JSON.stringify(error));
				 	  
				 	 $('.loading-spinner').hide();
				 			
				 	});

			}
			//End Create Ticket
});
// End Home Controller