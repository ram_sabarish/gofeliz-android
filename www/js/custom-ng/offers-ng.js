app.controller('offersController',function($scope,$http){
               
    $scope.pageClass = 'slideUp';


   $scope.retriveOffers = function(){

           	db.transaction(function(tx){

           					tx.executeSql('SELECT * FROM Offers', [], callback, null);

           			}, null);


           function callback(tx, result){

              var offerList = new Array();

              for( i = 0 ; i < result.rows.length ; i++){

                  var row = result.rows.item(i);

                  var expires = getExpiresDate(row['ExpiresIn']+'');

                  var offer = { "code": row['PromoCode'],
                                "title": row['Title'],
                                "details": row['Details'],
                                "expires": expires };

                  offerList.push(offer);
              }

              $scope.$apply(function() {

                  $scope.offerList = offerList;

              });
           }

   }

   $scope.retriveOffers();

});