app.controller('loginController',function($scope,$http){
	
	 $scope.pageClass = 'login';
	 $scope.step = 'login';
	 $scope.phoneNumberError = false;
	 $scope.passwordError = false;
	 $scope.emailError = false;
	 	 
	 /*Login Submit*/
	 $scope.submit = function(){
		 
		 $scope.phoneNumberBlur(); //Invoked for validation. 
		 
		 $scope.passwordBlur(); //Invoked for validation.
		 
		 if( $scope.phoneNumberError || $scope.passwordError)
			 return;
		 
		 $(".loading-spinner").show();

		 var user_data =  {			
					username: $scope.phoneNumber,
					password: $scope.password
					};

		 $scope.login(user_data);
		 
		 
	 }
	 /*End Of Login Submit.*/
	 
	 
	 /*Function to login */
	 $scope.login = function(user_data){
		 
		 $http.post(hostname + '/ws/user/login', user_data ).
		 then( function(response){
			        
			        $(".loading-spinner").hide();
			        
			        if(response.statusText == 'OK'){
			        	
			        	$http.defaults.headers.common['X-CSRF-Token']= response.data.token;
					        	
			        	isUserLoggedIn = true;
			        	user_info = new Object();
				        user_info.uid = response.data.user.uid;
				        user_info.firstName = response.data.user.field_first_name.und[0].value;
				        user_info.lastName = response.data.user.field_last_name.und[0].value;
				        user_info.email = response.data.user.mail;
				        user_info.phoneNumber = user_data.username;
				        user_info.password = user_data.password;
				        
				        storeUserCredentials();
				        goBack();
			        }
			        
			        
			        
			         
		         },
		         function(error){
		        	 
		        	     alert(JSON.stringify(error));
		                 
		        	     $(".loading-spinner").hide();
		           
		                 if(error.data[0].substring(0,14) == 'Already logged')
		                	 $scope.reLogin(user_data);
		});
		 
	 }
	 /*End of function login*/
	 
	 
	 
	 /*Function to relogin*/
	 $scope.reLogin = function(user_data){
		 
		 $http.post( hostname + '/ws/user/logout', user_data).
         then(function(response){
        	 
        	// alert(JSON.stringify(response));
        	
            $scope.login(user_data);
        	 
         },function(error){
        	 alert(JSON.stringify(error));
        	 
         });
		 
		 
	 }
	 /*Function relogin*/
	 
	 /*Validation on phone number. Invoked on phone number blur, when focus change to some other input field.*/
	 $scope.phoneNumberBlur = function(){
		 
		 if(!regixPhone.test($scope.phoneNumber+"")) 
			 $scope.phoneNumberError = true;
	 }
	 /*End Of phone number validation.*/
	 
	 
	 /*Invoked when focus is on phone number input*/	 
	 $scope.phoneNumberFocus = function(){
		 
		 $scope.phoneNumberError = false;
	 }
	 /*End of phone number focus.*/
	 
	 
	 /*Password Validation. Invoked on password blur.*/
	 $scope.passwordBlur = function(){
		 
		 if($scope.password == undefined)
			 $scope.passwordError = true;
		 
		 else if(($scope.password+"").length < 6)
			 $scope.passwordError = true;
	 }
	 /*End of Password Validation.*/
	 
	 
	 /*Invoked when focus is on password.*/
	 $scope.passwordFocus = function(){
		 
		 $scope.passwordError = false;
	 }
	 /*End of password focus.*/
	 
	 
	 /*Invoked on Sign Up phone number submit*/
	 $scope.submitSignUpPhonenumber = function(){
		 
		 $scope.phoneNumberBlur(); //Invoked for validation.
		 
		 if( $scope.phoneNumberError)
			 return;
		 
		 var user_data = { user_details:{
			
			      phone : $scope.phoneNumber
		     }		 
		 };
		 
		 
		 $http.post(hostname + '/ws/feliz/user/otav',user_data).
		 then( function(response){
			   alert(JSON.stringify(response));
			   
		       }, function(error){
			 alert(JSON.stringify(error));
		 });
		 
		 $scope.step = 'signup-next';
 
	 }
	 /*End of Sign Up phone number submit*/
	 
	/*Validate Email. Invoked on email blur.*/ 
	$scope.emailBlur = function(){
		
	  if( $scope.email == undefined )
		  $scope.emailError = true;
		
	  else if( !regixEmail.test( $scope.email + "" ) )
			$scope.emailError = true;
	}  
	/*End of email blur.*/
	 
	
	/*Invoked on email focus*/
	$scope.emailFocus= function(){
		
		$scope.emailError = false;
	}
	/*End email focus.*/
	
	
	/*SignUp User Details Submit */
	$scope.submitSignUpUserDetails = function(){
		
		 var user_data =  { user_details:{
			 	otav: $scope.otav,
				phone: $scope.phoneNumber, 
				firstName: $scope.firstName,
				lastName: $scope.lastName,
				email: $scope.email
			}
         };
		 		 		 
		$http.post(hostname + '/ws/feliz/user/regselflogin', user_data).
		then(function(response){
			   alert(JSON.stringify(response));
		     },
		     function(error){
		       alert(JSON.stringify(error));	 
		     }); 		
	}
	/*End SignUP User Submit*/
	
	
	/*Forgot Password*/
	$scope.forgotPassword = function(){
		
		$scope.phoneNumberBlur(); //Invoked for validation.
		
		if( $scope.phoneNumberError)
			 return;
		
		var data = {
				user_details : {
					phone :  $scope.phoneNumber
				}
			};
		alert('1');
		// send request to server to generate otp , pass on phone as parameter     
		$http.post(hostname+ '/ws/feliz/user/otap', data).
							then(function(response) {	
								
								alert(JSON.stringify(response));
								
								/*
								if(response.data.Status=="Success"){	    		 
									return {status:"success",result:"success"};
					    	    }else{
					    	    	return {status:"success",result:"otp-failed"};
					    	    }*/
						    	
						    },function(errorResponse){	   
						      // called asynchronously if an error occurs
						      // or server returns response with an error status.
						    	
						    	alert(JSON.stringify(errorResponse));
						    });
		alert('2');
			

	}
	/*End Forgot Password*/
});
/*******************End Of Login Controller.********************/