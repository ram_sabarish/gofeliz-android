app.controller('orderDetalsController',function($scope,$http){
	
	var timeList = [ { 'id':'0', 'shift':'Morning','value':'9:00 AM - 11:00 AM'},
	                 { 'id':'1', 'shift':'Late Morning','value':'11:00 AM - 1:00 PM'},
	                 { 'id':'2', 'shift':'After Noon','value':'1:00 PM - 3:00 PM'},
	                 { 'id':'3', 'shift':'Late After Noon','value':'3:00 PM - 5:00 PM'},
	                 { 'id':'4', 'shift':'Evening','value':'5:00 PM - 7:00 PM'},
	                 { 'id':'5', 'shift':'Late Evening','value':'7:00 PM - 9:00 PM'}];
	
	
	//Get Category Name
	$scope.getCategoryName = function(id){
		
		switch(parseInt(id)){
		
		  case 40: return "Plumbing";
		
		  case 190: return "Electricals";
		
		  case 256: return "Carpentry";
	
		}	
		
		return "None";
	}
	//End Get Category Name
	
	
	
	//Get Order Details
	$scope.getOrderDetails = function(){
	
		var url = [ hostname, "/ws/entity_node?parameters[type]=service_ticket&parameters[uid]=", user_info.uid,
		            "&parameters[nid]=", subCategoryNID,
		            "&fields=title,nid,field_ticket_status,field_service_description,created,field_add_landmark,field_building_flat_house_no_,field_street,field_city_area,field_scheduled_time,field_scheduled_date,field_service_main_category,field_service_sub_category,field_lead_source,field_coupon"].join("");
		
		$('.loading-spinner').show();
		
		$http.get(url, {}).then(function(response) {
            
	  		   //$('.loading-spinner').hide();
	  		   
	  		   //alert(JSON.stringify(response));
	  		   
	  		   //alert(JSON.stringify(response.data[0].field_coupon));
	  		   
	  		   $scope.title = response.data[0].title;
	  		   $scope.status = response.data[0].field_ticket_status.und[0].value;
	  		   $scope.category = $scope.getCategoryName(response.data[0].field_service_main_category.und[0].target_id);
	  		  
	  		   
	  		   
	  		   
	  		   if(response.data[0].field_service_description != undefined && response.data[0].field_service_description.und != undefined){
	  			 $scope.description = response.data[0].field_service_description.und[0].value;   
	  		   }
	  		  
	  	       $scope.landmark = response.data[0].field_add_landmark.und[0].value;
	  	       $scope.city_area = $scope.getCityName(response.data[0].field_city_area.und[0].target_id);
	  	       $scope.building_flat_house_no = response.data[0].field_building_flat_house_no_.und[0].value;
	  	       $scope.street = response.data[0].field_street.und[0].value;
	  	       $scope.scheduled_date = response.data[0].field_scheduled_date.und[0].value;
	  	       $scope.scheduled_time = response.data[0].field_scheduled_time.und[0].value;
	  		   
	  	       if(response.data[0].field_service_sub_category != undefined && response.data[0].field_service_sub_category.und != undefined){
		  		
	  	    	    // $scope.subCategory = response.data[0].field_service_sub_category.und[0].target_id; 
		  			 
		  			 $scope.checkSubCategories( response.data[0].field_service_main_category.und[0].target_id, response.data[0].field_service_sub_category.und);
		  	    
	  	       }else{
		  	   
	  	    	  $('.loading-spinner').hide();
	  	 	      $scope.showDetails = true; 
		  	   }
	         
	          },
	          function(error){
	         
	             $('.loading-spinner').hide();
	          	
	             //alert(JSON.stringify(error));
	             
	        }); 
	}
	//End Get Order Details
	
	
	//Get Formatted Date
    $scope.getFormattedDate = function(dateString){
 	
       var createdDate = new Date(dateString);
 	   
 	   return [ getDayNameUtilites(createdDate), createdDate.getDate(), getMonthNameUtilities(createdDate), [createdDate.getYear(),""].join("").substring(1,3) ].join(" ") ;
 	   
    }
   //End Get Formatted Date
    
  //Get Formatted Time
    $scope.getFormattedTime = function(id){
    	
    	for( i = 0; i < timeList.length ; i++){
    		
    	  if( timeList[i].id == id ){
    		  
    		  return  timeList[i].value;
    		}
    	}	
 	
       return "None";
    }
   //End Get Formatted Time
    
    
   //Get City Area Name 
    $scope.getCityName = function(nid){
    	
    	for( i = 0; i < cityList.length; i++){
    		
    		if( cityList[i].nid == nid){
    			return cityList[i].title;
    		}
    	}
    	
    	return nid;
    }
   //End Get City Area Name 
    
    
  //Get Check Categories
	$scope.checkSubCategories = function( categoryNid, subCategories){
		
		switch(parseInt(categoryNid)){
		
		  case 40: // "Plumbing"
			       if( plumbingSubCategory != null ){
			    	   $scope.getSubCategoriesName( subCategories, plumbingSubCategory );
			    	   $scope.showDetails = true;
			      	   return;
			       }
			       else{
			    	   plumbingSubCategory = new Array();
			    	   $scope.getSubCategories(plumbingSubCategory, categoryNid, subCategories);
			       }
			       
			       break;
		
		  case 190: // "Electrical"
			  		if( electricalsSubCategory != null ){
			  			$scope.getSubCategoriesName( subCategories, electricalsSubCategory );
			  			$scope.showDetails = true;
			  			return;
			  		}
			  		else{
			  			electricalsSubCategory = new Array();
			  			$scope.getSubCategories(electricalsSubCategory, categoryNid, subCategories);
			  		}
			  		
			        break;
		
		  case 256: // "Carpentery"
			  		if( carpenterSubCategory != null ){
			  			$scope.getSubCategoriesName( subCategories, carpenterSubCategory );
			  			$scope.showDetails = true;
			  			return;
			  		}
			  		else{
			  			carpenterSubCategory = new Array();
			  			$scope.getSubCategories(carpenterSubCategory, categoryNid, subCategories);
			  		}
		  		
			  		break;
	
		}
	 
	}
	//End Get Sub Categories 
	
	
	//Get Sub Categories Name
	$scope.getSubCategoriesName = function(subCategories, plumbingSubCategory){
		
		var subCategoriesName = '';
		
		for( i = 0; i < subCategories.length; i++){
			
			for( j = 0; j < plumbingSubCategory.length; j++){
				
				if( subCategories[i].target_id == plumbingSubCategory[j].nid){
					subCategoriesName = [subCategoriesName, plumbingSubCategory[j].title, ', '].join('');
				}
			}
		}
		
		$scope.subCategory = subCategoriesName;
		$('.loading-spinner').hide();
 	    $scope.showDetails = true; 
	}
	//End Get Sub Categories Name
	
	
	//Get Sub Categories
	$scope.getSubCategories = function(subCategoryArray, nid, subCategories){
						
		var url = [ hostname, '/ws/entity_node?parameters[field_main_category]=',  nid, '&parameters[type]=service_sub_category&fields=nid,title,field_main_category'].join("");
	
		 $http.get(url,{}).
         then(function(response){
        	       //alert(JSON.stringify(response));
        	       if(response.statusText = 'OK'){
        	    	   var subCategory;
        	    	           	    	   
        	    	   for( i = 0 ; i < response.data.length ; i++){
        	    		   subCategory = new Object();
        	    		   subCategory.title = response.data[i].title;
        	    		   subCategory.nid =response.data[i].nid;
        	    		   subCategoryArray.push(subCategory);
        	    	   }
        	    	   
        	    	   $scope.getSubCategoriesName(subCategories, subCategoryArray);     	    	          	    	   
        	    	   
        	       }
               },
               
               function(error){
            	   $('.loading-spinner').hide();
            	  // alert(JSON.stringify(error)); 
         });
	
	}
	//End Get Sub Categories
	
	//Show Ticket Details
	$scope.showTicketDetails = function(){
		   
		   $scope.title = orderNo;
		   $scope.status = 'Open';
		   $scope.category = $scope.getCategoryName(ticketDetails.field_service_main_category.und[0].target_id);
		   
		   if(ticketDetails.field_service_description != undefined && ticketDetails.field_service_description.und != undefined){
	  			 $scope.description = ticketDetails.field_service_description.und[0].value;   
	  		   }
		   
		   $scope.landmark = ticketDetails.field_add_landmark.und[0].value;
  	       $scope.city_area = $scope.getCityName(ticketDetails.field_city_area.und[0].target_id);
  	       $scope.building_flat_house_no = ticketDetails.field_building_flat_house_no_.und[0].value;
  	       $scope.street = ticketDetails.field_street.und[0].value;
  	       
  	       $scope.scheduled_date = ticketDetails.field_scheduled_date.und[0].value;
	       $scope.scheduled_time = ticketDetails.field_scheduled_time.und[0].value;
	       
	       if(ticketDetails.field_service_sub_category != undefined && ticketDetails.field_service_sub_category.und != undefined){
		  		
 	    	    // $scope.subCategory = response.data[0].field_service_sub_category.und[0].target_id; 
	  			 
	  			 $scope.checkSubCategories( ticketDetails.field_service_main_category.und[0].target_id, ticketDetails.field_service_sub_category.und);
	  	   
	       }else{
	  	    
	    	   $scope.showDetails = true;
	  	   
	       }
		   
		   
		   orderNo = ticketDetails = null;
		   
	}
	//End Ticket Details
	
	
	if(ticketDetails == null){
		 $scope.getOrderDetails();
	}    
	else{
		
		 $scope.showTicketDetails();
		 //alert(JSON.stringify(ticketDetails));
	}
	
});
//End Of orderDetalsController