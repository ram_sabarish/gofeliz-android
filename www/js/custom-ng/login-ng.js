app.controller('loginController',function($scope,$http){
	
	 
	 $scope.step='signIn';
	 	 
	 if( animate ){
		 $scope.pageClass = 'slideDown';
	 }
	 else{
		 animate = true; 
	 }
	 
	 $scope.showTabContent = function(tabNo){
		 
		 $('.custom-tab-header-options').removeClass('custom-tab-header-active');
		 
		 
		 switch(tabNo){
		 
		 case 1: $('#signIn').addClass('custom-tab-header-active');
		         $scope.step='signIn';
			     break;
		 
		 case 2: $('#signUp').addClass('custom-tab-header-active');
		         $scope.step='signUp';
			     break;
		 }
	 }
	 //End showTabContent
	 
	 //Login
	 $scope.login = function(user_data){
		 
		 $(".loading-spinner").show();

		 if(user_data == undefined){
			 
			user_data =  {	
					        username: $scope.phoneNumber,
						    password: $scope.password
						 };
		 }
		 

		 $http.post( [ hostname, '/ws/user/login' ].join(""), user_data ).
		 then( function(response){
			        
			        $(".loading-spinner").hide();
			        
			        if(response.statusText == 'OK'){
			        	
			        	$http.defaults.headers.common['X-CSRF-Token']= response.data.token;
					        	
			        	islogin = true;
			        	
			        	user_info = new Object();
				        user_info.uid = response.data.user.uid;
				        user_info.firstName = response.data.user.field_first_name.und[0].value;
				        user_info.lastName = response.data.user.field_last_name.und[0].value;
				        user_info.email = response.data.user.mail;
				        user_info.phoneNumber = user_data.username;
				        user_info.password = user_data.password;
				        
				        storeUserCredentials();   
				        
				        if(ticketDetails == null){
				          
				        	goBack();
				        }
				        else{
				        	
				        	ticketDetails.uid = user_info.uid;
				        	$scope.bookTicket();
				        }
				        
			        }
		      },
		      function(error){
		               
		       	  if(error.data[0].substring(0,14) == 'Already logged')
	                	 $scope.reLogin(user_data);
		       	  
		       	  else{
		       		  
		       		$(".loading-spinner").hide();
		       		$scope.passwordError = "Either phone number or password is incorrect.";
		       		//alert(JSON.stringify(error));
		       		
		       	  }
		 });
		 
	 }
	 //End Login
	 
	 /*Function to relogin*/
	 $scope.reLogin = function(user_data){
		 
		 $http.post( hostname + '/ws/user/logout', user_data).
         then(function(response){
        	 
        	// alert(JSON.stringify(response));
        	
            $scope.login(user_data);
        	 
         },function(error){
        	// alert(JSON.stringify(error));
        	 
         });
		 
		 
	 }
	 /*Function relogin*/
	 
	 
	 //Forgot Password
	 $scope.forgotPassword = function(){
		 
		 
         if(!regixPhone.test($scope.phoneNumber+"")){
			 
			 $scope.phoneNumberError = 'Enter a valid 10 digit phone number.';
			 
			 return;
		 }
         else{
        	 
        	 $scope.phoneNumberError = false;
         }
 
		 var data = {
					user_details : {
						phone :  $scope.phoneNumber
					}
				};

			$(".loading-spinner").show();

			// send request to server to generate otp , pass on phone as parameter     
			$http.post(hostname+ '/ws/feliz/user/otap', data).
								then(function(response) {	

                                    $(".loading-spinner").hide();

									if(response.data.Status == 'Success'){

									  $scope.step='signIn';

                                      $("#success-msg").show();

									}
									else{

									    $scope.phoneNumberError = "Sorry, we didn't recognise this phone number.";
									}
									

                                    
                                    
							    	
							    },function(errorResponse){	   
							     
							         $(".loading-spinner").hide();
							    	//alert(JSON.stringify(errorResponse));
							    });
	 }
	 //End Forgot Password
	 
	 /*Invoked on Sign Up phone number submit*/
	 $scope.submitSignUpPhonenumber = function(){
					 
		 var user_data = { user_details:{
			
			      phone : $scope.phoneNumberSignUp
		     }		 
		 };
		 
		 $(".loading-spinner").show(); 
		 
		 $http.post( [ hostname, '/ws/feliz/user/otav'].join(""),user_data).
		 then( function(response){
			 //alert(JSON.stringify(response));
			   $(".loading-spinner").hide(); 
			   
		       }, function(error){
			 //alert(JSON.stringify(error));
			 $(".loading-spinner").hide(); 
		 });
 
	 }
	 /*End of Sign Up phone number submit*/
	 
	 
	 //Register User
	 $scope.registerUserWithOTAV = function(){
	
		 var user_data =  { user_details:{
			 	otav: $scope.otav,
				phone: $scope.phoneNumberSignUp, 
				firstName: $scope.firstName,
				lastName: $scope.lastName,
				email: $scope.email
			}
         };
		 
		$(".loading-spinner").show();

		$scope.verificationCodeError = false;
		 
		$http.post([hostname, '/ws/feliz/user/regselflogin'].join(""), user_data).
		then(function(response){
			
			   $(".loading-spinner").hide(); 

			   if(response.data.Status != 'Error'){
				   
				   $http.defaults.headers.common['X-CSRF-Token']= response.data.token;

                   islogin = true;

                   user_info = new Object();
                   user_info.uid = response.data.user.uid;
                   user_info.firstName = response.data.user.field_first_name.und[0].value;
                   user_info.lastName = response.data.user.field_last_name.und[0].value;
                   user_info.email = response.data.user.mail;
                   user_info.phoneNumber = $scope.phoneNumberSignUp;
                   user_info.password = null;

                   storeUserCredentials();

                   if(ticketDetails == null){

                   				        	goBack();
                   }
                   else{

                   	ticketDetails.uid = user_info.uid;
                   	$scope.bookTicket();
                    }

			   }
			   else{

			      $scope.verificationCodeError = 'Invalid mobile verification code';
			   }
			   
		     },
		     function(error){
		      
		       $(".loading-spinner").hide(); 
		    	 
		       $scope.step = 'signUp';

               $scope.phoneNumberErrorSignUp = 'Sorry, this number is already registered.';

		     }); 		
	 }
	//End Register User
	 
	 //Validate Login
	 $scope.validatelogin = function(){
		 
		 var validation = true;
		 
		 if(!regixPhone.test($scope.phoneNumber+"")){
			 
			 $scope.phoneNumberError = 'Enter a valid 10 digit phone number.';
			 
			 validation = false;
		 }
		 else{
			 
             $scope.phoneNumberError = false;
		 }
		 
		 if(($scope.password+"").length < 6){
			 
			 $scope.passwordError = 'Password length too short (6 minimum).';
			 
			 validation = false;
		 }
		 else{
			 
			 $scope.passwordError = false;	
		 }
			
		 
		 if(validation){
			 
			 $scope.login();
		 }	 
	 }
	 //End Validate Login
	 
	 
	 //Validate SignUP
	 $scope.validateSignUp = function(){
		 
		 var validation = true;
		 
         if(!regixPhone.test($scope.phoneNumberSignUp+"")){
			 
			 $scope.phoneNumberErrorSignUp = 'Enter a valid 10 digit phone number.';
			 
			 validation = false;
		 }
		 else{
			 
             $scope.phoneNumberErrorSignUp = false;
		 }
		 
		 
		 if(validation){
			 $scope.step='verification'; 
			 $scope.submitSignUpPhonenumber();
		 }
		 
	 }
	 //End Validate SignUP
	 
	 
	 
	 //Book Ticket
	 $scope.bookTicket = function(){
		 
		 $('.loading-spinner').show();
			
			$http.post( [hostname, '/ws/entity_node'].join(""), ticketDetails)
			  .then(function(response) {
			    					  			  
				  if(response.statusText = 'OK'){
				  
				    if(response.data.field_ticket_status.und[0].value == 'Open'){
				    	
				      $('.loading-spinner').hide();
						  
				      orderNo = response.data.title;
				      
				      newRequestAdded = true;

				      window.history.go(-3); 	
				    }	 
				  
				  }
				  
			 	},function(error){
			 		
			 	  //alert(JSON.stringify(error));
			 	  
			 	  $('.loading-spinner').hide();
			 			
			 	});
	 }
	 //End Book Ticket
});	 