app.controller('accountController',function($scope, $http){
               
               $scope.pageClass = 'slideUp';
               $scope.userLoggedIn = islogin;
               animate = false;
               
               if(islogin){
            	   
            	   $scope.uid = user_info.uid;
            	   $scope.firstName = user_info.firstName;
            	   $scope.lastName = user_info.lastName ;
            	   $scope.email = user_info.email;
            	   $scope.phoneNumber = user_info.phoneNumber;
            	   $scope.password = user_info.password;	
               }     
               
               $scope.onEdit = function(){
            	               	   
            	   $scope.editMode = true;
            	   $scope.changePassword = $scope.firstNameError = $scope.lastNameError = $scope.emailError = $scope.passwordError = $scope.newPasswordError = $scope.passwordMismatch = false; 
            	    
            	   $scope.firstNameNew = $scope.firstName;
            	   $scope.lastNameNew = $scope.lastName;
            	   $scope.emailNew =  $scope.email;
            	   $scope.currentPassword = $scope.confirmPassword = $scope.newPassword="";
            	   
            		   
               }
               
               //Update Account
               $scope.updateAccountDetails = function(){
            	               	              	
            	   var updateFlag = false;
            	   
            	   var user_data = new Object();
            	   
            	   if( $scope.firstNameNew != $scope.firstName){
            		   
            		   user_data.field_first_name = {
                			   
                			   "und" : [
                			       {
                			    	 "value": $scope.firstNameNew 
                			       }      
                			    ]
                	   };
            		   
            		   updateFlag = true;
            	   }
            	   
            	   if( $scope.lastNameNew != $scope.lastName ){
            		   
            		   user_data.field_last_name = {
                			   
                			   "und" : [
                			       {
                			    	 "value": $scope.lastNameNew 
                			       }      
                			    ]
                	   };
            		   
            		   updateFlag = true;
            	   }
            	   
            	   if( $scope.emailNew !=  $scope.email ){
            		  
            		   user_data.mail = $scope.emailNew;
            		   
            		   updateFlag = true;
            	   } 	     
            	   
            	   if($scope.changePassword){
            		   
            		   user_data.pass = $scope.newPassword;
            		   
            		   updateFlag = true; 
            	   }
            	   
            	   user_data.current_pass = $scope.password;
            	            	   
            	   
            	   if(updateFlag){
            		
            		   $('.loading-spinner').show();
            		   
            		   $http.put( [ hostname, '/ws/user/', $scope.uid].join(""), user_data).
                	   then( function(response){
                		   
                		     // alert(JSON.stringify(response));
                		   
                		      $('.loading-spinner').hide();
                		      
                		      if(response.statusText == 'OK'){
                		    	  
                		    	 $scope.editMode = false;
                		    	 
                		    	 user_info.firstName = $scope.firstName = $scope.firstNameNew;
                		    	 
                		    	 user_info.lastName = $scope.lastName = $scope.lastNameNew;
                		    	 
                		    	 user_info.email = $scope.email = $scope.emailNew;
                		    	 
                		    	 if( $scope.changePassword ){
                		    		 
                		    		 user_info.password = $scope.password = $scope.newPassword;
                		    	 }
                		    	 
                		    	 storeUserCredentials(); 
                		      }
                		      
                	        },
                	        function(error){  	       	        	
                	        	
                	        	$('.loading-spinner').hide();
                	        	
                	        	//alert(JSON.stringify(error));
                	   });
            		   
            	   }else{
            		   
            		   $scope.editMode = false; 
            	   }
            	   
               }
               //End Update Account 
               
               
               //Validate Account Details
               $scope.validateDetails = function(){
            	   
            	   $scope.firstNameError = $scope.lastNameError = $scope.emailError = $scope.passwordError = $scope.newPasswordError = $scope.passwordMismatch = false; 
            	   
            	   
            	   if( $scope.firstNameNew == "" || $scope.firstNameNew == undefined){
            		   
            		   $scope.firstNameError = "Please enter first name.";
            		   
            		   return;
            	   }
            	   
            	   if( $scope.lastNameNew == "" || $scope.lastNameNew == undefined){
            		   
            		   $scope.lastNameError = "Please enter last name.";
            		   
            		   return;
            	   }
            	   
            	   
                   if( !regixEmail.test( $scope.emailNew)){
            		   
            		   $scope.emailError = "e.g. (UserName@DomainName.com).";
            		   
            		   return;
            	   }
            	   
            	   if( ($scope.password != null) && ($scope.password != $scope.currentPassword) ){
            		   
            		   $scope.passwordError = "Please enter correct password."; 
            		   
            		   return;
            	   }    	  
            	   
            	   if( $scope.changePassword && ( $scope.newPassword == "" || $scope.newPassword == $scope.currentPassword || $scope.newPassword == undefined ) ){
            		   
            		   $scope.newPasswordError = "Please enter new password."; 
            		   
            		   return;
            	   }
            	   
            	   if( $scope.changePassword && $scope.newPassword != $scope.confirmPassword){
            		   
            		   $scope.passwordMismatch = "Password doesn't match.";
            		   
            		   return;
            	   }
            	   
            	   $scope.updateAccountDetails();
            	   
               }
              //End Validate Account Details
            
               
            $scope.onLoginToggle = function(){
          		 
          		 if( $scope.userLoggedIn ){
          			 $('#logout').show();
          			 $('#login-overlay').show();
          		 }
          		           		 
          	 }
          	 
          	 //Logout
          	 $scope.onLogout = function(){
          		 
          		 var user_data =  {			
          					username:  user_info.phoneNumber,
          					password: user_info.password
          					};

          		 serviceRequestList = null;

          		 $(".loading-spinner").show();
          		 		 
          		 $http.post( hostname + '/ws/user/logout', user_data).
                   then(function(response){
                  	 
                  	 //alert(JSON.stringify(response));
                  	 
                  	 $scope.userLoggedIn = false;
                  	
                  	 $('#logout').hide();
              		 $('#login-overlay').hide();
              		 $(".loading-spinner").hide();
              		 
              		 removeUserCredentials();
              		 goBack();
                  	 
                   },function(error){
                  	 
                  	 //alert(JSON.stringify(error));
                  	 
                  	 $('#logout').hide();
              		 $('#login-overlay').hide();
              		 $(".loading-spinner").hide();
              		 
              		 $scope.userLoggedIn = false;
                   	        	 		 
              		 removeUserCredentials();
              		goBack();
                  	 
                   });
          		 
          	 }
          	 //End Logout
          	 
          	 //Remove overlay & hide Logout
          	 $scope.hideLougout = function(){
          		 $('#logout').hide();
          		 $('#login-overlay').hide();
          	 }
          	 //End Remove overlay & hide Logout
          	    
});
//End Controller