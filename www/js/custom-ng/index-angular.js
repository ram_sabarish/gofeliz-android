//var hostname = "https://www.gofeliz.com";
var hostname = "http://gofelizraz9vuvrw9.devcloud.acquia-sites.com";
var isUserLoggedIn;
var user_info;
var categories;
var timeSlots= [{value:'Morning (8:00-10:00 AM)'},{value:'Evening (4:00-6:00 PM)'}];
var dates;
var cities;
var cityAreas;
var app = angular.module('gofeliz',['ngRoute','ngAnimate']);

app.config(function($routeProvider){
           
           $routeProvider.when('/',{
                               
                               templateUrl: 'html/Home.html',
                               controller: 'homeController'
                               
                        }).when('/Account',{
                                
                                templateUrl: 'html/Account.html',
                                controller: 'accountController'
                                
                        }).when('/Orders',{
                                        
                                templateUrl: 'html/Orders.html',
                                controller: 'ordersController'
                                        
                        }).when('/Settings',{
                                        
                                templateUrl: 'html/Settings.html',
                                controller: 'settingsController'
                                        
                        }).when('/login',{
                        	   
                        	    templateUrl: 'html/LoginSignup.html',
                        	    controller: 'loginController' 
                        	  
                        });
           
});



app.controller('settingsController',function($scope){
               
               $scope.pageClass = 'Settings';
               
});


app.controller('mainController',function($scope,$http){
	
	var authdata = Base64.encode('internal' + ':' + 'gofeliz2015');
	
	$http.defaults.headers.common['Authorization'] = 'Basic ' + authdata;
	
	initializePage();
	
	
	/*Check User Logged in status*/
	if(window.localStorage.uid == undefined){
	    isUserLoggedIn = false;
	}
	else{
		restoreUserCredentials();
	}
    /*End Check User Logged in status*/

	/*Get session token, categories, time slots, cities & city areas*/
	$http.post(hostname + "/services/session/token",{}).
	then(function(response){
			
		$http.defaults.headers.common['X-CSRF-Token']= response.data;
		
		return $http.get(hostname + '/ws/entity_node?parameters[type]=service_category&fields=field_service_category_image,nid,title', {});
	}).
	then(function(response){
				
		if(response.statusText = 'OK'){
			
			var category;
			
			categories = new Array();
						
			for( i = 0 ; i < response.data.length ; i++ ){
			
				category = new Object();
				
				category.title = response.data[i].title;
				
				category.nid = response.data[i].nid;
				
				categories.push(category); 
			}	
		}
		
		
		return $http.post(hostname + '/ws/feliz/user/tslots',{});
	}).
	then(function(response){
		
		if(response.statusText = 'OK')
			timeSlots = response.data;	
		
		return $http.get(hostname + '/ws/entity_node?parameters[type]=city&parameters[title]=Pune&fields=nid,title',{});		
	}).
	then(function(response){
		
		if(response.statusText = 'OK')
			cities = response.data;
		
		return $http.get(hostname +'/ws/entity_node?parameters[field_city_city]='+cities[0].nid+'&parameters[type]=city_areas&fields=nid,title', {});

	}).
	then(function(response){
		
		if(response.statusText = 'OK')
			cityAreas = response.data;
		
		$('.loading-spinner').hide();
		
	}).
	catch(function(error){
		
		$('.loading-spinner').hide();
		
		alert(JSON.stringify(error));
	});
	/*End Get session token, categories, time slots, cities & city areas*/
	
	
	
	/*Set time slots
     timeSlots = [{"id":0, "available":1, "startsFrom": 8, "endsAt": 10, "val1": "Early Morning", "val2": "8:00-10:00 AM", "value":"Early Morning (8:00 - 10:00 AM)"},
                  {"id":1, "available":1, "startsFrom": 10, "endsAt": 12,  "val1": "Late Morning", "val2": "10:00-12:00 Noon", "value":"Late Morning (10:00 - 12:00 Noon)"},
                  {"id":2, "available":1, "startsFrom": 12, "endsAt": 14,  "val1": "After Noon", "val2": "12:00-2:00 PM", "value":"After Noon (12:00- 2:00 PM)"},
                  {"id":3, "available":1, "startsFrom": 14, "endsAt": 16,  "val1": "Late After Noon", "val2": "2:00-4:00 PM", "value":"Late After Noon (2:00 - 4:00 PM)"},
                  {"id":4, "available":1, "startsFrom": 16, "endsAt": 18,  "val1": "Evening", "val2": "4:00-6:00 PM", "value":"Evening (4:00 - 6:00 PM)"},
                  {"id":5, "available":1, "startsFrom": 18, "endsAt": 20,  "val1": "Late Evening", "val2": "6:00-8:00 PM", "value":"Late Evening (6:00 - 8:00 PM)"},
                  {"id":6, "available":1, "startsFrom": 20, "endsAt": 22,  "val1": "Night", "val2": "8:00-10:00 PM", "value":"Night (8:00 - 10:00 PM)"}];

	End Set time slots*/
     
   /*Set Date*/ 
     var dateNow = new Date();   
     dates = new Array();
               
     for( i = 0 ; i < 8 ; i++){
    	 	     	     	     	 
    	 dates.push(new Date( dateNow.getTime() + i * 60 * 60 * 24 * 1000));
     }
     
   /*End Set Date*/   
});
/*End mainController*/