app.controller('settingsController',function($scope,$http){
               
    $scope.pageClass = 'slideUp';
    $scope.aboutUsUrl = 'https://www.gofeliz.com/page/about-us';
	$scope.careersUrl = 'https://www.gofeliz.com/page/careers';
	$scope.contactUsUrl = 'https://www.gofeliz.com/contact-us';
	$scope.FAQSUrl = 'https://www.gofeliz.com/page/faq';
	$scope.feedbackUrl = 'https://www.gofeliz.com/feedback';
	$scope.trustNSafetyUrl = 'https://www.gofeliz.com/page/trust-and-safety';
	$scope.websiteUrl = 'https://www.gofeliz.com/#/ticket/cat';
    
	
	$scope.openInAppBrowser = function(url){
		 
		 //window.open(url, '_blank', 'location=no'); 
				
		 $('#inAppBrowser-overlay').show();
			 
		 $(".loading-spinner").show();
			 
		 var popup = window.open(url, "_blank", "location=no,toolbar=yes,hidden=yes");
		
		 popup.addEventListener("loadstop", function() {
								  			  
			  $(".loading-spinner").hide();
				  
			  popup.show();	 

		});

	}
    //End openInAppBrowser	 	 
});