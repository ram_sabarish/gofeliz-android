app.controller('homeController',function($scope,$location,$http,$filter){
	 $scope.pageClass = 'home';
	 $scope.isLogin = islogin;
	
	 $scope.imageList = [{ "path" : "img/background-city.jpg" },
	                     { "path" : "img/background-home.jpg" },
	                     { "path" : "img/background-night.jpg" },
	                     { "path" : "img/background-table.jpg" }, 
	                     { "path" : "img/background-work-bench.jpg" }];
	 animate = true;
	           
     //alert($scope.internet);
    
     
	 if(orderNo != null){

		 $location.path('OrderDetail');
	 }
	 
	 if($scope.isLogin){
		 
		 $scope.fisrtName = user_info.firstName;
	 }
		
	 if(internetAvailable){
		 
		 $('#no-internet').hide();
	     $('#home-page').show();
	     
	 }else{
		 
		 $('#home-page').hide();
	     $('#no-internet').show();
	 }
	 
	 $scope.selectedCategory= function(categoryOption){
		 
		 category = categoryOption;	 
	 }
	 
	 
	 //Hide Popup
	 $scope.hideOrderPopup = function(){
		 $('#order-success-popup').hide();
		 $('#order-success-popup-overlay').hide();
	 }
	 //End Hide Popup
	 
	 //Image Click
	 $scope.imageClick = function(action){
		 
		 //alert(action);
		 
		 if(action == "" || action == "<blank>"){
			 
			 //alert('Oops, something went wrong!');
			 
			 return;
		 }
			 
		 
		 $('#inAppBrowser-overlay').show();
		 
		 $(".loading-spinner").show();
			 
		 var popup = window.open(action, "_blank", "location=no,toolbar=yes,hidden=yes");
		
		 popup.addEventListener("loadstop", function() {
								  			  
			  $(".loading-spinner").hide();
			  
			  $('#inAppBrowser-overlay').hide();
				  
			  popup.show();	 

		});
 
	 }
	 //End Image Click
	 
	 //Get Offer Images
	 $scope.getOfferImages = function(){
		
		 $http.get('http://www.gofeliz.com/ws/entity_node?parameters[type]=mobile_teasers&fields=nid,title,field_slide_show_image,field_slide_show_image_target',{}).
		 then(function(response){
			 
			 //alert(JSON.stringify(response));
			 
			 if(response.statusText = 'OK'){
				 
				 offerImages = new Array();
				 
				 var image;
				 var uri;
				 
				 for( i = 0; i < response.data.length; i++ ){
					 
					 if( response.data[i].field_slide_show_image != undefined && response.data[i].field_slide_show_image.und != undefined &&
							 response.data[i].field_slide_show_image_target != undefined && response.data[i].field_slide_show_image_target.und != undefined ){
						 
						 image = new Object();
						 
						 uri = response.data[i].field_slide_show_image.und[0].uri; 
						 image.uri = uri.replace("public://", "https://www.gofeliz.com/sites/default/files/");
						
						 image.action = response.data[i].field_slide_show_image_target.und[0].value;
						 
						 offerImages.push(image);
					 }
					 
				 }
				 
				 if(offerImages.length > 0){
					// $scope.slides2 = offerImages;
					 
					 var slidesImages = new Array();
					 
					 var temp;
					 
					 for( i = 0; i < 50; i++){
						 
						 for( j = 0; j < offerImages.length; j++){
							 
							 temp = new Object();
							 temp.uri = offerImages[j].uri;
							 temp.action = offerImages[j].action;
							 
							 slidesImages.push( temp);
						 }
					 }	 
					 
					 $scope.slides2 = offerImages = slidesImages;
					 
					 $scope.showSlides = true;

				 }
				 //alert(JSON.stringify(offerImages));
			 }
			 
		 },function(error){
			 
			// alert(JSON.stringify(error));
		 });
		 
	 }
	 //End Offer Images
	 
	 
	 $scope.onSocialClick = function(){
		 
		 if($('#social-float').hasClass('social-active')){
			 $('#social-float').removeClass('social-active');
			 $('#social-float').addClass('social-inactive');
			 
			 $('.social-option').removeClass('social-option-active');
			 $('.social-option').addClass('social-option-inactive');

			 $('#arrow-tag-2').removeClass('arrow-tag-inactive');
             $('#arrow-tag-2').addClass('arrow-tag-active');
             
		 }else{
			 $('#social-float').removeClass('social-inactive');
			 $('#social-float').addClass('social-active');
			 
			 $('.social-option').removeClass('social-option-inactive');
			 $('.social-option').addClass('social-option-active');

			 $('#arrow-tag-2').removeClass('arrow-tag-active');
             $('#arrow-tag-2').addClass('arrow-tag-inactive');
		 }

	 }
	 
	 
	 $scope.onFacebookClick = function(){
 
		 $('#inAppBrowser-overlay').show();
		 
		 $(".loading-spinner").show();
			 
		 var popup = window.open('https://www.facebook.com/gofeliz', "_blank", "location=no,toolbar=yes,hidden=yes");
		
		 popup.addEventListener("loadstop", function() {
								  			  
			  $(".loading-spinner").hide();
			  
			  $('#inAppBrowser-overlay').hide();
				  
			  popup.show();
		});
	 }
	 //End onFacebookClick
	 

	 $scope.onGoogleClick = function(){

		 $('#inAppBrowser-overlay').show();
		 
		 $(".loading-spinner").show();
			 
		 var popup = window.open('https://plus.google.com/111823694079258938621', "_blank", "location=no,toolbar=yes,hidden=yes");
		
		 popup.addEventListener("loadstop", function() {
								  			  
			  $(".loading-spinner").hide();
			  
			  $('#inAppBrowser-overlay').hide();
				  
			  popup.show();	 

		});
		 
	}
	//End onGoogleClick
	 
	 $scope.onTwitterClick = function(){
		 
		 $('#inAppBrowser-overlay').show();
		 
		 $(".loading-spinner").show();
			 
		 var popup = window.open('https://twitter.com/gofelizcom', "_blank", "location=no,toolbar=yes,hidden=yes");
		
		 popup.addEventListener("loadstop", function() {
								  			  
			  $(".loading-spinner").hide();
			  
			  $('#inAppBrowser-overlay').hide();
				  
			  popup.show();	 

		});
	 }
	 //End onTwitterClick


	 $scope.onClickShare = function(){

	   //window.plugins.socialsharing.share("Hey! I'm using GoFeliz. An amazing app for on-demand professional home services. I would highly recommend you to give it a try...",'GoFeliz',null,'https://www.gofeliz.com/');
	   //window.plugins.socialsharing.shareVia('com.facebook.katana',"Hey! I'm using GoFeliz. An amazing app for on-demand professional home services. I would highly recommend you to give it a try...",null,null,'https://www.gofeliz.com/',null,null);
	   //window.plugins.socialsharing.shareVia('com.google.android.apps.plus',"Hey! I'm using GoFeliz. An amazing app for on-demand professional home services. I would highly recommend you to give it a try...",null,null,'https://www.gofeliz.com/',null,null);
	   //window.plugins.socialsharing.shareVia('com.whatsapp',"Hey! I'm using GoFeliz. An amazing app for on-demand professional home services. I would highly recommend you to give it a try...",null,null,'https://www.gofeliz.com/',null,null);
       //window.plugins.socialsharing.shareVia('com.twitter.android',"Hey! I'm using GoFeliz. An amazing app for on-demand professional home services. I would highly recommend you to give it a try...",null,null,'https://www.gofeliz.com/',null,null);

       $scope.onSocialClick();
       $('#share-options').show();
       $('#share-options-overlay').show();
	 }
	 //End of Social Share


	 //Hide Share Options
	 $scope.hideShareOptions = function(){
	   $('#share-options').hide();
	   $('#share-options-overlay').hide();
	 }
	 //End of Hide Share Options


     //Share via Facebook
	 $scope.shareFacebook = function(){

      $(".loading-spinner").show();
      $('#share-options').hide();
      $('#share-options-overlay').hide();

	  window.plugins.socialsharing.shareVia('com.facebook.katana',"Hey! I'm using GoFeliz. An amazing app for on-demand professional home services. I would highly recommend you to give it a try...",
	                                        null, null, 'https://www.gofeliz.com/',
	                                        function(){ $(".loading-spinner").hide(); },
	                                        function(err){ $(".loading-spinner").hide(); });
	 }

     //Share via Google+
     $scope.shareGoogle = function(){

     $(".loading-spinner").show();
     $('#share-options').hide();
     $('#share-options-overlay').hide();

     window.plugins.socialsharing.shareVia('com.google.android.apps.plus',"Hey! I'm using GoFeliz. An amazing app for on-demand professional home services. I would highly recommend you to give it a try...",
     	                                        null, null, 'https://www.gofeliz.com/',
     	                                        function(){ $(".loading-spinner").hide(); },
     	                                        function(err){ $(".loading-spinner").hide(); });

     }

      //Share via Twitter
          $scope.shareTwitter = function(){

          $(".loading-spinner").show();
          $('#share-options').hide();
          $('#share-options-overlay').hide();

          window.plugins.socialsharing.shareVia('com.twitter.android',"Hey! I'm using GoFeliz. An amazing app for on-demand professional home services. I would highly recommend you to give it a try...",
          	                                        null, null, 'https://www.gofeliz.com/',
          	                                        function(){ $(".loading-spinner").hide(); },
          	                                        function(err){ $(".loading-spinner").hide(); });

          }


      //Share via WhatsApp
                $scope.shareWhatsApp = function(){

                $(".loading-spinner").show();
                $('#share-options').hide();
                $('#share-options-overlay').hide();

                window.plugins.socialsharing.shareVia('com.whatsapp',"Hey! I'm using GoFeliz. An amazing app for on-demand professional home services. I would highly recommend you to give it a try...",
                	                                        null, null, 'https://www.gofeliz.com/',
                	                                        function(){ $(".loading-spinner").hide(); },
                	                                        function(err){ $(".loading-spinner").hide(); });

                }

      //Share via SMS
      $scope.shareSMS = function(){
         $('#share-options').hide();
         $('#share-options-overlay').hide();
        window.plugins.socialsharing.shareViaSMS("Hey! I'm using GoFeliz. An amazing app for on-demand professional home services. I would highly recommend you to give it a try...", null, null,null);
      }

      $scope.hideUpdateNow = function(){
        $('#update-now-popup').hide();
        upgradeNow = false;
      }

	 if(offerImages == null){
		 
		
		 $scope.getOfferImages();
	 }
	 else{
		 
		 $scope.slides2 = offerImages;
		 
		 $scope.showSlides = true;
	 }

	 if(upgradeNow == false){
	   $('#update-now-popup').hide();
	 }
	 
});//End homeController


  /*
		 $http.defaults.headers.common['Content-Type']= 'application/json';
		 $http.defaults.headers.common['Authorization']= 'key=AIzaSyCSJtX9qy1ahLEHAcu0xl2-irU_pOA4NfY';

		 $http.post("https://gcm-http.googleapis.com/gcm/send",
				 { "to": "/topics/gofeliz_promo",
			     "data": {
				    "message": "This is a GCM Topic Message!",
				   }
		 }).
		 then(function(response){
			 alert(JSON.stringify(response));
		 },function(error){
			 alert(JSON.stringify(error));
		 });



		var data = {  "message": "This is a GCM Topic Message!",
                               "code":"5577","title":"Rs.200","details":"An Amazing Deal.","expires":"Feb 30"};

		insertOffers(data);


		$http.post("https://www.gofeliz.com/ws/push_notifications",
                 		    { "token": registrationToken,
                 		      "type": "android"
                 		 }).
                 		 then(function(response){
                 			 alert(JSON.stringify(response));
                 		 },function(error){
                 			 alert(JSON.stringify(error));
                 		 });

   */